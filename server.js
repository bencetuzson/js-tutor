'use strict';

const Express = require("express");
const Cors = require('cors');
const Morgan = require('morgan');

const App = Express();
const PORT = "8080";

let thread = [{
    Author: "Miki",
    Content: "Yolo World",
}];

App.use(Morgan('combined'));

App.use(Cors());

App.get("/", (req, res) => {res.send(JSON.stringify(thread))});

App.get("/posts", (req, res) => {res.send(JSON.stringify(thread))});

App.post("/post", (req, res) => {
    let postContent = req.body;
    console.log(postContent);

    let Parsed = JSON.parse(postContent);
     
    thread.push(Parsed);
});

App.listen(PORT, () => {console.log(`web-tutor server listening on PORT ${PORT}`)});
